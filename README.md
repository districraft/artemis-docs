<div align=center>![Artemis Logo](/static/artemis_docs.png)</div>

# Artemis Documentation

This is the public documentation for the
[Artemis](https://gitlab.com/districraft/artemis) project.

The latest build of this documentation can be found 
[here](https://artemis.docs.districraft.org).

## Contributing
 1. Read [CONTRIBUTING.md]
 2. Fork this repository and clone it locally. We're using
    a submodule for the theme so clone with ```--recursive```.
 3. Edit the appropriate page. Refer to the 
    [Hugo documentation](https://gohugo.io/documentation/)
    if you're unsure how to proceed.
 4. There is currently no automated spell-checking process
    so please run your edits through your favourite
    spellchecker before you commit.
 5. Commit your changes with a descriptive message outlining
    the changes you've made.
 6. Submit a pull request. See [CONTRIBUTING.md] for more
    info.

To build the documentation, run ```./bin/hugo```. Then open 
```index.html``` from the ```public/``` directory.

## Documentation License
Copyright (C) 2020 Jakub Sycha, Districraft and Contributors.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
A copy of the license is included in the file LICENSE.txt.

## License regarding code samples
Copyright (C) 2020 Jakub Sycha, Districraft and Contributors.

All code examples provided with this documentation are published
under the GPLv3 license. See GPLV3.txt for more details.
