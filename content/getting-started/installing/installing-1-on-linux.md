+++
title = "Installing on Linux"
chapter = false
+++

{{% notice info %}}
There is an easier way to install Artemis [using docker](/getting-started/running/running-1-docker/). Only follow this guide if you can't or don't want to use Docker.
{{% /notice %}}

### Prerequisities
 - Java 8 or newer ([Amazon Corretto](https://aws.amazon.com/corretto/) is recommended)
 - MySQL 5 or newer (PerconaDB, MariaDB or other MySQL Compatible DBMS, Postgres might work but is not supported)
 - NGINX or other web server that can act as a Reverse Proxy

### Getting Artemis
Download [artemis.jar](https://nexus.districraft.org/repository/jar/artemis/_latest/artemis.jar) from the [releases server](https://nexus.districraft.org/service/rest/repository/browse/jar/artemis/_latest/).