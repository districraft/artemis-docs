+++
title = "Running Locally"
chapter = false
+++

{{% notice info %}}
**Artemis is NOT production ready!**
{{% /notice %}}

## On Linux
### Prerequisities
 - Java 8 or newer ([Amazon Corretto](https://aws.amazon.com/corretto/) is recommended)
 - MySQL 5 or newer (PerconaDB, MariaDB or other MySQL Compatible DBMS, Postgres might work but is not supported)
 - NGINX or other web server that can act as a Reverse Proxy

### Running Artemis
#### Set up the Databse
 - ```CREATE DATABASE artemis;```
 - ```CREATE USER 'artemis'@'localhost' IDENTIFIED WITH mysql_native_password BY 'artemis-password';```
 - ```GRANT ALL PRIVILEGES ON artemis.* TO 'artemis'@'localhost';```
 - ```FLUSH PRIVILEGES;```

#### Configure Artemis
 - ```cp src/main/resources/application.properties.example src/main/resources/application.properties```
 - Edit the ```application.properties``` file. See Global Prerequisities to get your API tokens.

#### Set up your proxy
Artemis uses a reverse proxy to serve both the "back-office" and the CMS homepage from the same app. NGINX is recommended.


##### NGINX
 - Open ```/etc/nginx/sites-available/artemis.conf``` in your favourite text editor
 - Put the following text in thew newly created site config:

```
server {
        listen 80;
        listen [::]:80;

        server_name devsite.local;

        location / {
                proxy_pass http://localhost:8080/cms/;
        }

        location ~* \.(js|css|html|txt|png|jpg|jpeg)$ {
                proxy_pass http://localhost:8080;
        }
}

server {
        listen 80;
        listen [::]:80;

        server_name artemis.devsite.local;

        location / {
                proxy_pass http://localhost:8080;
        }

        location /cms$ {
                return 302 http://devsite.local;
        }
}
```

 - Run ```ln -s /etc/nginx/sites-available/artemis.conf /etc/nginx/sites-enabled/artemis.conf```
 - Check your config with ```nginx -t```
 - Reload nginx with ```systemctl reload nginx``` or ```/etc/init.d/nginx reload``` depending on your distro

##### Apache
Apache is not officialy supported and this might not work.

```
<VritualHost *:80>
    ServerName devsite.local
    
    ProxyPass / http://loclhost:8080/cms
    ProxyPassReverse / http://loclhost:8080/cms
</VirtualHost>

<VritualHost *:80>
    ServerName artemis.devsite.local
    
    ProxyPass / http://loclhost:8080/
    ProxyPassReverse / http://loclhost:8080/
</VirtualHost>
```

#### Edit your hosts file
Put the following line in your ```hosts``` file

```127.0.0.1 devsite.local artemis.devsite.local```

#### Run Artemis
 - ```./mvnw clean compile spring-boot:run```

 The Artemis installer will start automatically on the first run. To run it again, run the following in your database and restart artemis: 
 
 ```sql
 UPDATE artemis_config SET conf_val = 0 WHERE conf_key = 'installed';
 ```

 Unless you disable this option in your ```application.properties``` file the spring autoreload will be enabled by default. When you edit the source, rebuild your code (CTRL+F9 in Idea) and Artemis will automatically reload the changes.

 ## On Windows and macOS
 This process is pretty much identical on Windows and macOS.

 ### Windows specific stuff
 It's recommended to install NGINX through [WSL](https://docs.microsoft.com/en-us/windows/wsl/install-win10) when running artemis on Windows.

 ### macOS specirfic stuff
 TBD