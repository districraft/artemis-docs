+++
title = "Global Prerequisities"
chapter = false
+++

## API Tokens
Artemis uses MTCaptcha as a CAPTCHA service. You will need to [sign up for an API key](https://admin.mtcaptcha.com/signup?plantype=A) to use it.
Put your public and private keys in your ```application.properties``` file.