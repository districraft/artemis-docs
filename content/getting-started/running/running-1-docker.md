+++
title = "Running with Docker"
chapter = false
+++

{{% notice info %}}
**Artemis is NOT production ready!** Docker is currently the best way to serve it in a production-like environment. It is however not well suited
for development as there is currently no way to access the code running within the Docker container. For development, [run Artemis locally](/getting-started/running/running-1-locally/)
{{% /notice %}}

## On Linux
### Prerequisities
 - Linux kernel 3.10 or newer (latest kernel provided by your distro is recommended!)
 - Docker 19.03 or newer
 - Docker Compose 1.26.0 or newer
 - GNU Make or compatible

 ### Running Artemis with Docker
  1. Run ```make prepare-docker```
  2. Edit ```/docker/artemis/docker-application.properties```
  3. Run ```make docker```
  4. Add ```127.0.0.1 devsite.local artemis.devsite.local``` to your ```hosts``` file
  5. Open http://artemis.devsite.local in your browser
  6. Complete the installation wizard
  7. Open http://devsite.local to see the CMS

The hosts file edit is required to be able to access the CMS

The Artemis installer will start automatically on the first run.

{{% notice info %}}
There is a non-zero chance the MySQL container will fail. If this happens, run ```make clean-docker``` and redeploy with ```make docker```
{{% /notice %}}

## On Windows

{{% notice warning %}}
**Docker deployment of Artemis is not supported on Windows.** [Local deployment](/getting-started/installing/installing-1-locally/) is recommended.
{{% /notice %}}

**To Docker deploy Artemis on Windows, install [Docker for Windows](https://docs.docker.com/docker-for-windows/), [MINGW](http://www.mingw.org/) and follow the Linux steps.**