# Contributing

When contributing to this repository, please first discuss the change you wish to make via issue,
email, or any other method with the owners of this repository before making a change.

## Pull Request Process

 1. Ensure your changes are functional by running ```bin/hugo serve``` and reviewing the page(s)
    you have edited in your web browser by pointing it to ```http://localhost:1313```.
 2. Update the CHANGELOG.md with details of your changes.
 3. If you have developer access to the repository, you will need an approval from two other
    developers or the maintainer to be able to merge.
 4. If you don't have developer access, your Merge Request will be reviewed and merged by
    the maintainer or a developer at their nearest convenience.

## Legal
By submitting your edits to this repository, you agree to the following:

 - You retain ownership of the Copyright in Your Contribution and have the same rights to use
   or license the Contribution which You would have had without entering into the Agreement.
 - To the maximum extent permitted by the relevant law, You grant to Us a perpetual, worldwide,
   non-exclusive, transferable, royalty-free, irrevocable license under the Copyright covering 
   the Contribution, with the right to sublicense such rights through multiple tiers of 
   sublicensees, to reproduce, modify, display, perform and distribute the Contribution as 
   part of the Material.
 - Your edits will be published under the GNU Free Documentation License version 1.3

## Code of Conduct
**We don't really have one... Just don't be an ass :)**